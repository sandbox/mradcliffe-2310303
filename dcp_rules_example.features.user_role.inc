<?php
/**
 * @file
 * dcp_rules_example.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function dcp_rules_example_user_default_roles() {
  $roles = array();

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 3,
  );

  // Exported role: content reviewer.
  $roles['content reviewer'] = array(
    'name' => 'content reviewer',
    'weight' => 2,
  );

  return $roles;
}
