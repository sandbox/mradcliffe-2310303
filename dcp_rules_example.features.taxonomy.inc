<?php
/**
 * @file
 * dcp_rules_example.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function dcp_rules_example_taxonomy_default_vocabularies() {
  return array(
    'priority' => array(
      'name' => 'Priority',
      'machine_name' => 'priority',
      'description' => 'Priority levels for tasks',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'tags' => array(
      'name' => 'Tags',
      'machine_name' => 'tags',
      'description' => 'Use tags to group articles on similar topics into categories.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
