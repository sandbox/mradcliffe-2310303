<?php
/**
 * @file
 * dcp_rules_example.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function dcp_rules_example_default_rules_configuration() {
  $items = array();
  $items['rules_dcp_limit_to_one_membership'] = entity_import('rules_config', '{ "rules_dcp_limit_to_one_membership" : {
      "LABEL" : "DCP limit to one membership",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "TAGS" : [ "dcp" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "line_item" : { "label" : "Line Item", "type" : "commerce_line_item" } },
      "IF" : [
        { "entity_is_of_bundle" : {
            "entity" : [ "line-item" ],
            "type" : "commerce_line_item",
            "bundle" : { "value" : { "product" : "product" } }
          }
        },
        { "entity_is_of_bundle" : {
            "entity" : [ "line-item:commerce-product" ],
            "type" : "commerce_product",
            "bundle" : { "value" : { "membership" : "membership" } }
          }
        },
        { "data_is" : { "data" : [ "line-item:quantity" ], "op" : "\\u003E", "value" : "1" } }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "line-item:quantity" ], "value" : "1" } },
        { "entity_save" : { "data" : [ "line-item" ] } },
        { "drupal_message" : {
            "message" : "You may only purchase one membership at this time.",
            "type" : "warning",
            "repeat" : 0
          }
        }
      ]
    }
  }');
  $items['rules_dcp_notify_admin_of_critical_task'] = entity_import('rules_config', '{ "rules_dcp_notify_admin_of_critical_task" : {
      "LABEL" : "DCP: Notify admin of critical task",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "dcp" ],
      "REQUIRES" : [ "rules" ],
      "ON" : {
        "node_insert--task" : { "bundle" : "task" },
        "node_update--task" : { "bundle" : "task" }
      },
      "IF" : [
        { "entity_is_of_bundle" : {
            "entity" : [ "node:field-task-priority" ],
            "type" : "taxonomy_term",
            "bundle" : { "value" : { "priority" : "priority" } }
          }
        },
        { "data_is" : {
            "data" : [ "node:field-task-priority:field-priority-notification" ],
            "value" : 1
          }
        }
      ],
      "DO" : [
        { "mail_to_users_of_role" : {
            "roles" : { "value" : { "3" : "3" } },
            "subject" : "([node:field-task-priority]) [node:title]",
            "message" : "A new critical task at [node:url] called [node:title]",
            "from" : [ "node:author:mail" ]
          }
        }
      ]
    }
  }');
  $items['rules_dcp_notify_reviewer_of_unpublished_content'] = entity_import('rules_config', '{ "rules_dcp_notify_reviewer_of_unpublished_content" : {
      "LABEL" : "DCP: Notify reviewer of unpublished content",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "dcp" ],
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_insert--article" : { "bundle" : "article" } },
      "IF" : [ { "NOT node_is_published" : { "node" : [ "node" ] } } ],
      "DO" : [
        { "mail_to_users_of_role" : {
            "roles" : { "value" : { "4" : "4" } },
            "subject" : "[site:name] New Unpublished Article For Review",
            "message" : "Hi,\\r\\n\\r\\nThere is new, unpublished article to review at [node:edit-url] by [node:author] created on [node:created].\\r\\n\\r\\n",
            "from" : [ "node:author:mail" ]
          }
        }
      ]
    }
  }');
  $items['rules_dcp_order_has_more_than_one_membership'] = entity_import('rules_config', '{ "rules_dcp_order_has_more_than_one_membership" : {
      "LABEL" : "DCP Order has more than one membership",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "dcp" ],
      "REQUIRES" : [ "commerce_order", "rules", "commerce_cart" ],
      "ON" : { "commerce_cart_product_add" : [] },
      "IF" : [
        { "commerce_order_contains_product_type" : {
            "commerce_order" : [ "commerce_order" ],
            "product_type" : { "value" : { "membership" : "membership" } },
            "operator" : "\\u003E=",
            "value" : "1"
          }
        },
        { "data_is" : {
            "data" : [ "commerce-line-item:quantity" ],
            "op" : "\\u003E",
            "value" : "1"
          }
        }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "commerce-line-item:quantity" ], "value" : "1" } },
        { "entity_save" : { "data" : [ "commerce-line-item" ], "immediate" : 1 } }
      ]
    }
  }');
  $items['rules_dcp_purchase_membership'] = entity_import('rules_config', '{ "rules_dcp_purchase_membership" : {
      "LABEL" : "DCP: Purchase Membership",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "dcp" ],
      "REQUIRES" : [ "commerce_order", "rules", "commerce_payment" ],
      "ON" : { "commerce_payment_order_paid_in_full" : [] },
      "IF" : [
        { "commerce_order_contains_product_type" : {
            "commerce_order" : [ "commerce_order" ],
            "product_type" : { "value" : { "membership" : "membership" } },
            "operator" : "\\u003E=",
            "value" : "1"
          }
        }
      ],
      "DO" : [
        { "LOOP" : {
            "USING" : { "list" : [ "commerce-order:commerce-line-items" ] },
            "ITEM" : { "list_item" : "Current list item" },
            "DO" : [
              { "component_rules_dcp_subscribe_member" : { "line_item" : [ "list-item" ], "user" : [ "commerce-order:owner" ] } }
            ]
          }
        }
      ]
    }
  }');
  $items['rules_dcp_subscribe_member'] = entity_import('rules_config', '{ "rules_dcp_subscribe_member" : {
      "LABEL" : "DCP: Subscribe Member",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "TAGS" : [ "dcp" ],
      "REQUIRES" : [ "rules", "og" ],
      "USES VARIABLES" : {
        "line_item" : { "label" : "Line Item", "type" : "commerce_line_item" },
        "user" : { "label" : "User", "type" : "user" }
      },
      "IF" : [
        { "entity_is_of_bundle" : {
            "entity" : [ "line-item" ],
            "type" : "commerce_line_item",
            "bundle" : { "value" : { "product" : "product" } }
          }
        },
        { "entity_is_of_bundle" : {
            "entity" : [ "line-item:commerce-product" ],
            "type" : "commerce_product",
            "bundle" : { "value" : { "membership" : "membership" } }
          }
        }
      ],
      "DO" : [
        { "og_subcribe_user" : {
            "user" : [ "user" ],
            "group" : [ "line-item:commerce-product:field-subscription-group" ]
          }
        }
      ]
    }
  }');
  return $items;
}
